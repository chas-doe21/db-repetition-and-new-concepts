# A query from `u05`

Here's an example of a query taken from `u05` Sprint 3:

```sql
SELECT products.name,
               SUM(inventory.quantity) + SUM(sold_products.quantity),
               stores.name
               FROM inventory
               JOIN products ON products.id = inventory.product
               JOIN stores ON stores.id = inventory.store
               JOIN sold_products ON sold_products.product = products.id
               WHERE products.name = 'Kattmat'
               GROUP BY stores.name, products.name;

```

Running it gives us:

```
  name   | ?column? |         name          
---------+----------+-----------------------
 Kattmat |      521 | Den Lilla Djurbutiken
 Kattmat |      777 | Den Stora Djurbutiken
 ```

 Given that the `id` for `'Kattmat'` is `02d9562f-05e8-49b2-8ace-6e166c440060`, let's check if the stock numbers are correct:

```sql
select * from inventory where product = '02d9562f-05e8-49b2-8ace-6e166c440060';
```

gives 

```
                  id                  |               product                | quantity |                store                 
--------------------------------------+--------------------------------------+----------+--------------------------------------
 18298375-7cb1-4c30-81c8-5a5b80e24a10 | 02d9562f-05e8-49b2-8ace-6e166c440060 |      355 | 676df1a1-f1d1-4ac5-9ee3-c58dfe820927
 172bb573-4724-431f-aaea-665cef6020d2 | 02d9562f-05e8-49b2-8ace-6e166c440060 |      227 | ff53d831-c2fe-4fe8-9f67-5d69118670f2
```

`ff53d831-c2fe-4fe8-9f67-5d69118670f2` is `'Den Lilla Djurbutiken'` while
`676df1a1-f1d1-4ac5-9ee3-c58dfe820927` is `'Den Stora Djurbutiken'`.

So we can see that the initial stocklevel was 355 units for "Stora" and 227 for
"Lilla".

Let's look at the sales:

```sql
select quantity, stores.name from sold_products JOIN sales ON sold_products.sale = sales.id JOIN stores ON stores.id = sales.store where product = '02d9562f-05e8-49b2-8ace-6e166c440060';
```

gives

```
 quantity |         name          
----------+-----------------------
       57 | Den Lilla Djurbutiken
       10 | Den Stora Djurbutiken
```

So the final total is:
- Lilla Djurbutiken: 227 - 57 = 170
- Stora Djurbutiken: 355 - 10 = 345

So why does our original query give a wrong answer?

```sql
SELECT products.name,
               SUM(inventory.quantity) + SUM(sold_products.quantity),
               stores.name
               FROM inventory
               JOIN products ON products.id = inventory.product
               JOIN stores ON stores.id = inventory.store
               -- Here the only constraint to join on sold_products is that products.id = sold_products.product. 
               -- It does not take different sales and stores into consideration.
               JOIN sold_products ON sold_products.product = products.id
               WHERE products.name = 'Kattmat'
               GROUP BY stores.name, products.name;

```

Here's a correct version of it:

```sql
SELECT products.name,
               SUM(inventory.quantity) - SUM(sold_products.quantity),
               stores.name
               FROM inventory
               JOIN products ON products.id = inventory.product
               JOIN stores ON stores.id = inventory.store
               JOIN sales ON sales.store = stores.id
               -- Here it joins sold_products on id = product 
               -- *and* sold_product.sale = sales.id.
               JOIN sold_products ON sold_products.product = products.id AND sold_products.sale = sales.id
               WHERE products.name = 'Kattmat'
               GROUP BY stores.name, products.name;
```

Which gives

```
  name   | ?column? |         name          
---------+----------+-----------------------
 Kattmat |      170 | Den Lilla Djurbutiken
 Kattmat |      345 | Den Stora Djurbutiken
```