-- Subqueries
--
-- Let's say we want to write a query that returns all the tasks that were
-- created before the task that has the highest number of annotations.
--
-- We could solve it with two queries:
--
-- 1. write a query to find the creation time for the task with most annotations
-- 2. write a query to find all the tasks that have a creation date lesser 
--    than what we found during step 1
--
-- A better way would be to use a *subquery*:

-- (note: edit the creation times for tasks otherwise the query will return an
-- empty result.)

SELECT * FROM tasks WHERE created < (
    SELECT created FROM
        (SELECT tasks.created,
                COUNT(annotations.id) AS notes
                FROM tasks
                JOIN annotations ON tasks.id = annotations.task
                ORDER BY notes DESC LIMIT 1));
