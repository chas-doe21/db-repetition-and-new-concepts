-- JOINs
--
SELECT tasks.description, tasks.created, annotations.annotation
       FROM tasks [INNER|LEFT]
       JOIN annotations ON tasks.id = annotations.task;
