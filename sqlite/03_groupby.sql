-- GROUP BY
--
SELECT tasks.description, tasks.created, annotations.annotation
       FROM tasks
       LEFT JOIN annotations ON tasks.id = annotations.task
       GROUP BY tasks.id;
