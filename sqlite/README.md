# SQLite

- [SQLite](#sqlite)
  - [Fast](#fast)
  - [Self-contained](#self-contained)
    - [Main database file](#main-database-file)
  - [The most used DB engine](#the-most-used-db-engine)
- [Full-featured](#full-featured)
- [High reliability](#high-reliability)
- [Our example DB](#our-example-db)
- [Where to go from here](#where-to-go-from-here)

from the [official site](https://sqlite.org):

SQLite is a C-language library that implements a **small**, **fast**, **self-contained**,
**high-reliability**, **full-featured**, SQL database engine. SQLite is the most used
database engine in the world. SQLite is built into all mobile phones and most
computers and comes bundled inside countless other applications that people use
every day.

## Fast

 Read/write latency for SQLite is competitive with read/write latency of individual files on disk. Often SQLite is faster. Sometimes SQLite is almost as fast.

## Self-contained

SQLite is "stand-alone" or "self-contained" in the sense that it has very few
dependencies. It runs on any operating system, even stripped-down bare-bones
embedded operating systems. SQLite uses no external libraries or interfaces
(other than a few standard C-library calls). The entire SQLite library is
encapsulated in a single source code file that requires no special facilities or
tools to build.

### Main database file

The complete state of an SQLite database is usually contained in a single file
on disk called the "main database file".

During a transaction, SQLite stores additional information in a second file
called the "rollback journal", or if SQLite is in WAL mode, a write-ahead log
file.

A "main database file" can reside in memory instead of disk (`:memory:`).

## The most used DB engine

SQLite is likely used more than all other database engines combined. Billions and billions of copies of SQLite exist in the wild. SQLite is found in:

- Every Android device
- Every iPhone and iOS device
- Every Mac
- Every Windows10 machine
- Every Firefox, Chrome, and Safari web browser
- Every instance of Skype
- Every instance of iTunes
- Every Dropbox client
- Every TurboTax and QuickBooks
- PHP and Python
- Most television sets and set-top cable boxes
- Most automotive multimedia systems
- Countless millions of other applications

Since SQLite is used extensively in every smartphone, and there are more than 4.0 billion (4.0e9) smartphones in active use, each holding hundreds of SQLite database files, it is seems likely that there are over one trillion (1e12) SQLite databases in active use.

# Full-featured

Do not be misled by the "Lite" in the name. SQLite has a full-featured SQL
implementation.

More info [here](https://sqlite.org/fullsql.html).

# High reliability

SQLite is open-source but it is not open-contribution. All the code in SQLite is
written by a small team of experts. The project does not accept "pull requests"
or patches from anonymous passers-by on the internet.

The developers of SQLite intend to support the product through the year 2050. To
this end, the source code is carefully documented to promote long-term
maintainability.

# Our example DB

We'll use a test DB to explore SQLite (and some SQL-concepts that might not have
come up earlier in the course).

Here's the requirements for the DB:

- a table where to store tasks. A task has:
  - an `id` to uniquely identify it
  - a `description` which is the text of the task

Each task have the possibility to have one or more annotations connected to it.

- a table to store the annotations. An annotation has:
  - an `id` to uniquely identify it
  - an `annotation` which is the note text itself

Each entity in the DB (task, annotation) should record the date and time of its
creation.

Each modification or deletion of an entity should get recorded in the db, as to
be able to reconstruct the entity's history afterwards.

# Where to go from here

Check out [w3resource](https://www.w3resource.com/sqlite-exercises/)'s SQLite
exercises for some homework.