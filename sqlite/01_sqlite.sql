-- Here is a simple SQLite schema and seed data to showcase SQLite.
--

-- Use PRAGMA to turn foreign keys on (off by default)
-- PRAGMA foreign_keys=ON;
BEGIN TRANSACTION;

CREATE TABLE tasks (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    description TEXT NOT NULL, 
                    created DATETIME NOT NULL DEFAULT current_timestamp);

INSERT INTO tasks (description) VALUES ("This is a task"), ("This is another task"), ("This is yet another task");

CREATE TABLE annotations (id INTEGER PRIMARY KEY AUTOINCREMENT,
                          task INTEGER NOT NULL,
                          annotation TEXT NOT NULL,
                          created DATETIME NOT NULL DEFAULT current_timestamp,
                          FOREIGN KEY(task) REFERENCES tasks(id));

INSERT INTO annotations (task, annotation) VALUES (1, "Annotate task 1"),
                                                  (2, "Annotate task 2"),
                                                  (1, "Another note for task 1"),
                                                  (99, "This shouldn't be valid!"); -- This row violates the FOREIGN KEY constraint. Will we get an error?

-- Part of our requirements is that history of edits/delete for tasks and
-- annotations should be kept in the db.
--
-- The following tables will be used to store that.

CREATE TABLE tasks_hist (id INTEGER PRIMARY KEY AUTOINCREMENT,
                         task INTEGER NOT NULL,
                         description TEXT NOT NULL,
                         created DATETIME NOT NULL,
                         timestamp DATETIME NOT NULL DEFAULT current_timestamp,
                         FOREIGN KEY(task) REFERENCES tasks(id));

CREATE TABLE annotations_hist (id INTEGER PRIMARY KEY AUTOINCREMENT,
                               annotation INTEGER NOT NULL,
                               annotation_text TEXT NOT NULL,
                               task INTEGER NOT NULL,
                               created DATETIME NOT NULL,
                               timestamp DATETIME NOT NULL DEFAULT current_timestamp,
                               FOREIGN KEY(annotation) REFERENCES annotations(id),
                               FOREIGN KEY(task) REFERENCES tasks(id));

COMMIT;
