-- TRIGGERs
--
BEGIN TRANSACTION;

CREATE TRIGGER tasks_update_trg AFTER UPDATE ON tasks
        BEGIN
            INSERT INTO tasks_hist (task, description, created)
            VALUES (old.id, old.description, old.created);
        END;

CREATE TRIGGER tasks_delete_trg AFTER DELETE ON tasks
        BEGIN
            INSERT INTO tasks_hist (task, description, created)
            VALUES (old.id, old.description, old.created);
        END;

CREATE TRIGGER annotations_update_trg AFTER UPDATE ON annotations
        BEGIN
            INSERT INTO annotations_hist (annotation, annotation_text, task, created)
            VALUES (old.id, old.annotation, old.task, old.created);
        END;

CREATE TRIGGER annotations_delete_trg AFTER DELETE ON annotations
        BEGIN
            INSERT INTO annotations_hist (annotation, annotation_text, task, created)
            VALUES (old.id, old.annotation, old.task, old.created);
        END;

COMMIT;
